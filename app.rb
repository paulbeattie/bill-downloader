require 'sinatra'
require 'slim'
require_relative 'lib/bill_downloader'

get '/' do
  service = BillService::DownloadService.execute
  slim :index, locals: { bill: service.bill }
end
