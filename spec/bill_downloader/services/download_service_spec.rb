require 'spec_helper'

describe BillService::DownloadService do

  let (:json_body) {  "{}" }

  before do
    @stub_request = stub_request(:get, 'http://safe-plains-5453.herokuapp.com/bill.json').to_return(body: json_body)
    @downloader = BillService::DownloadService.execute
  end

  describe "#execute" do

    it "Requests the correct url" do
      expect(@stub_request).to have_been_requested
    end

    it "stores the correct data" do
      expect(@downloader.data).to eq("{}")
    end

  end

  describe "#bill" do
    it "returns a bill presenter" do
      expect(@downloader.bill).to be_a(BillService::BillPresenter)
    end
  end
end
