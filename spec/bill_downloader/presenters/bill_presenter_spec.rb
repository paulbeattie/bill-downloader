require 'spec_helper'

describe BillService::BillPresenter do
  let(:bill) { BillService::BillPresenter.new(bill_json) }

  describe "#due_date" do
    let (:bill_json) { { 'statement' => { 'due' => '2015-01-25' } } }
    it 'returns bill date' do
      expect(bill.due_date).to eq('25 Jan 2015')
    end
  end

  describe "#generated_at" do
    let (:bill_json) { { 'statement' => { 'generated' => '2015-01-11' } } }
    it 'returns statement generation date' do
      expect(bill.generated_at).to eq('11 Jan 2015')
    end
  end

  describe "#period_starts_on" do
    let (:bill_json) { { 'statement' => { 'period' => { 'from' => '2015-01-26' } } } }
    it 'returns statement from date' do
      expect(bill.period_starts_on).to eq('26 Jan 2015')
    end
  end

  describe "#period_ends_on" do
    let (:bill_json) { { 'statement' => { 'period' => { 'to' => '2015-02-25' } } } }
    it 'returns statement to date' do
      expect(bill.period_ends_on).to eq('25 Feb 2015')
    end
  end

  describe "#total" do
    let (:bill_json) { { 'total' => '136.03' } }
    it 'returns bill total' do
      expect(bill.total).to eq('£136.03')
    end
  end

  describe "#packages_total" do
    let (:bill_json) { { 'package' => { 'total' => '71.40' } } }
    it 'returns packages total' do
      expect(bill.packages_total).to eq('£71.40')
    end
  end

  describe "#call_charges_total" do
    let (:bill_json) { { 'callCharges' => { 'total' => '59.64' } } }
    it 'returns call charges total' do
      expect(bill.call_charges_total).to eq('£59.64')
    end
  end

  describe "#sky_store_total" do
    let (:bill_json) { { 'skyStore' => { 'total' => '24.97' } } }
    it 'returns sky store total' do
      expect(bill.sky_store_total).to eq('£24.97')
    end
  end

  describe "#packages" do
    subject { bill.packages }
    let (:bill_json) { { 'package' => { 'subscriptions' => [ { 'type' => 'Sky Talk Anytime', 'cost' => '5.0' }, {'type' => 'Variety with Movies HD', 'cost' => '50.0'} ] } } }
    it 'parses the correct amount of packages' do
      expect(subject.length).to eq(2)
    end

    it 'parsed the items into PackagePresenters' do
      expect(subject.first).to be_a(BillService::PackagePresenter)
    end
  end

  describe "#calls" do
    subject { bill.calls }
    let (:bill_json) { { 'callCharges' => { 'calls' => [ { 'called' => '07716393769', 'duration' => '00:23:03', 'cost' => '2.13' }, { 'called' => '07716393769', 'duration' => '00:23:03', 'cost' => '2.13' } ] } } }
    it 'parses the correct amount of calls' do
      expect(subject.length).to eq(2)
    end

    it 'parsed the items into CallPresenters' do
      expect(subject.first).to be_a(BillService::CallPresenter)
    end
  end

  describe "#rentals" do
    subject { bill.rentals }
    let (:bill_json) { { 'skyStore' => { 'rentals' => [ { 'title' => '50 Shades of Grey', 'cost' => '4.99'} ] } } }
    it 'parses the correct number of rentals' do
      expect(subject.length).to eq(1)
    end

    it 'parsed the items into SkyStorePresenters' do
      expect(subject.first).to be_a(BillService::SkyStorePresenter)
    end
  end

  describe "#buy_and_keep" do
    subject { bill.buy_and_keep }
    let (:bill_json) { { 'skyStore' => { 'buyAndKeep' => [ { 'title' => "That's what she said", 'cost' => '9.99' }, { 'title' => 'Broke back mountain', 'cost' => '9.99' } ] } } }
    it 'parses the correct amount of buy and keep items' do
      expect(subject.length).to eq(2)
    end

    it 'parsed the items into SkyStorePresenters' do
      expect(subject.first).to be_a(BillService::SkyStorePresenter)
    end
  end

end
