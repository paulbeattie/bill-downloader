require 'spec_helper'

describe BillService::PackagePresenter do
  let(:bill) { BillService::PackagePresenter.new(bill_json) }

  describe "#type" do
    let (:bill_json) { { 'type' => 'tv' } }
    it 'returns correct package type' do
      expect(bill.type).to eq('tv')
    end
  end

  describe "#name" do
    let (:bill_json) { { 'name' => 'Variety with Movies HD' } }
    it 'returns correct package name' do
      expect(bill.name).to eq('Variety with Movies HD')
    end
  end

  describe "#cost" do
    let (:bill_json) { { 'cost' => '50.00' } }
    it 'returns correct package cost' do
      expect(bill.cost).to eq('£50.00')
    end
  end

end
