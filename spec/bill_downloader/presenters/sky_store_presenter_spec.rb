require 'spec_helper'

describe BillService::SkyStorePresenter do
  let(:bill) { BillService::SkyStorePresenter.new(bill_json) }

  describe "#title" do
    let (:bill_json) { { 'title' => "That's what she said" } }
    it 'returns correct rental name' do
      expect(bill.title).to eq("That's what she said")
    end
  end

  describe "#cost" do
    let (:bill_json) { { 'cost' => '9.99' } }
    it 'returns correct rental cost' do
      expect(bill.cost).to eq('£9.99')
    end
  end

end
