require 'spec_helper'

describe BillService::CallPresenter do
  let(:bill) { BillService::CallPresenter.new(bill_json) }

  describe "#called" do
    let (:bill_json) { { 'called' => '07716393769' } }
    it 'returns correct number called' do
      expect(bill.called).to eq('07716393769')
    end
  end

  describe "#duration" do
    let (:bill_json) { { 'duration' => '00:23:03' } }
    it 'returns correct Call duration' do
      expect(bill.duration).to eq('00:23:03')
    end
  end

  describe "#cost" do
    let (:bill_json) { { 'cost' => '2.13' } }
    it 'returns correct Call cost' do
      expect(bill.cost).to eq('£2.13')
    end
  end

end
