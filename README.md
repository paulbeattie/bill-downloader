# Sky Bill Test
This is a basic Sinatra app which parses the JSON and renders this out to the user.

## Installation
The easiest way to install this is to first install bundler and then run `bundle install`

## Starting server
When you've ran a bundle install you can simple run `thin start` and this will launch the server, it should now be running on http://localhost:3000/
