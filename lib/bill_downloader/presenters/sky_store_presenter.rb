module BillService
  class SkyStorePresenter < Support::BasePresenter

    ##
    # Returns the title of the sky store item
    def title
      @json['title']
    end

    ##
    # Returns the cost of the sky store item formatted as GBP
    def cost
      format_as_money @json['cost']
    end

  end
end
