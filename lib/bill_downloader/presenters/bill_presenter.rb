module BillService
  class BillPresenter < Support::BasePresenter

    ##
    # Returns a formatted bill due date
    def due_date
      format_date @json['statement']['due']
    end

    ##
    # Returns a formatted statement generated date
    def generated_at
      format_date @json['statement']['generated']
    end

    ##
    # Returns a formatted period starting date
    def period_starts_on
      format_date @json['statement']['period']['from']
    end

    ##
    # Returns a formatted period ends date
    def period_ends_on
      format_date @json['statement']['period']['to']
    end

    ##
    # Returns a total cost of the overall bill formatted as GBP
    def total
      format_as_money @json['total']
    end

    ##
    # Returns a total cost of the packages formatted as GBP
    def packages_total
      format_as_money @json['package']['total']
    end

    ##
    # Returns a total cost of the call charges formatted as GBP
    def call_charges_total
      format_as_money @json['callCharges']['total']
    end

    ##
    # Returns a total cost of the sky store charges formatted as GBP
    def sky_store_total
      format_as_money @json['skyStore']['total']
    end

    ##
    # Returns an array of PackagePresenter objects
    def packages
      @json['package']['subscriptions'].map do |package|
        PackagePresenter.new package
      end
    end

    ##
    # Returns an array of CallPresenter objects
    def calls
      @json['callCharges']['calls'].map do |call|
        CallPresenter.new call
      end
    end

    ##
    # Returns an array of SkyStore objects which contain rentals
    def rentals
      @json['skyStore']['rentals'].map do |rental|
        SkyStorePresenter.new rental
      end
    end

    ##
    # Returns an array of SkyStore objects which contain buy and keep items
    def buy_and_keep
      @json['skyStore']['buyAndKeep'].map do |buy_and_keep|
        SkyStorePresenter.new buy_and_keep
      end
    end

  end
end
