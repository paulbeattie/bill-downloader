module BillService
  class PackagePresenter < Support::BasePresenter

    ##
    # Returns the type of package
    def type
      @json['type']
    end

    ##
    # Returns the name of the package
    def name
      @json['name']
    end

    ##
    # Returns the cost of the package formatted as GBP
    def cost
      format_as_money @json['cost']
    end

  end
end
