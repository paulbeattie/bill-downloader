module BillService
  class CallPresenter < Support::BasePresenter

    ##
    # Returns the number called
    def called
      @json['called']
    end

    ##
    # Returns the call duration
    def duration
      @json['duration']
    end

    ##
    # Returns the cost of the call formatted as GBP
    def cost
      format_as_money @json['cost']
    end

  end
end
