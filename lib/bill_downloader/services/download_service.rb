require 'json'
require 'open-uri'
module BillService
  class DownloadService

    attr_reader :data

    ##
    # JSON Source data
    JSON_URL = 'http://safe-plains-5453.herokuapp.com/bill.json'

    def self.execute
      service = self.new
      service.execute
      service
    end

    ##
    # Reads the reponse body from the result of opening the URl and stores it in data
    def execute
      @data = open(JSON_URL).read
    end

    ##
    # Creates a new BillPresenter object with the parsed JSON data receive in #response
    def bill
      BillPresenter.new(parsed_json)
    end

    private

    def parsed_json
      JSON.parse(@data)
    end

  end
end
