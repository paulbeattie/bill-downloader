module Support
  ##
  # This class represents the minimum required for a presenter
  class BasePresenter

    ##
    # Creates a base presenter which stores JSON and has some helper methods

    def initialize(json)
      @json = json
    end

    private

    def format_as_money(amount)
      format '£%.2f', amount
    end

    def format_date(date)
      Date.parse(date).strftime('%-d %b %Y')
    end

  end
end
